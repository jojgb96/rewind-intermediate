package rewindIntermediate;

public class MainRewind {
	public static int add(int num1, int num2) {
		int result =  num1 + num2;
		return result;
	}	
	public static int minus(int num1,int num2) {
		int result = num1 - num2;
		return result;
	}
	public static int multiply(int num1,int num2) {
		int result  = num1 * num2;
		return result;
	}
	public static int divide(int num1,int num2) {
		int result = num1 /  num2;
		return result;
	}
	
	public static void main(String[] args) {
		add(1,2);
		minus(5,4);
		multiply(4, 6);
		divide(8, 2);

	}

}
