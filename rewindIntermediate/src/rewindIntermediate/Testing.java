package rewindIntermediate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Testing {
	MainRewind mainRewind =  new MainRewind();
	int hasilPenjumlahan1 = 3;
	int angka1 = 10;
	int angka2 = 5;
	int result = angka1+angka2;
	int result2 = angka1 / angka2;
	int result3 = angka1-angka2;
	@Test
	void testAssertEquals() {
		assertEquals(3, mainRewind.add(1, 2));
		
	}
	@Test
	void testAssertTrue() {
		assertTrue(mainRewind.add(angka1, angka2) == 15);
	}
	@Test
	void testAssertFalse() {
		assertFalse(mainRewind.divide(angka1, angka2) == 3);
	}
	@Test
	void testAssertFalse2() {
		assertFalse(mainRewind.minus(angka1, angka2) == 6);
	}
	@Test
	void testAssertThrow() {
		assertThrows(NumberFormatException.class, () -> {
		    Integer.parseInt("One");
		  });
	}
	

}
